$(document).ready(function() {

	$.get(
		'/SEOTool/rest/account/info',
		function(data)
		{
			$("#username").html(data.username);
			$("#email").html(data.email);
			$("#website").html(data.website);
		});
});