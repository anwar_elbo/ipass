$(document).ready(function() {
	$("#crawl").click(function() {
		var web = $('#website').val();
		$.get(
			'/SEOTool/rest/crawl/page/' + web,
			function(data) 
			{
				try {
					$("#content").html("<tr><td><h3>" + data.content.title + "</h3></td></tr>");	
					$(data.content.messages).each(function (i, c) {
						$("#content").append("<tr><td>" + c + "</td><td></td></tr>")
					});
					$("#content").append("<tr><td><b>Score: " + data.content.rate + "</b></td></tr>");
	
					$("#meta").html("<tr><td><h3>" + data.meta.title + "</h3></td></tr>");	
					$(data.meta.messages).each(function (i, c) {
						$("#meta").append("<tr><td>" + c + "</td></tr>")
					});
					$("#meta").append("<tr><td><b>Score: " + data.meta.rate + "</b></td></tr>");
	
					$("#image").html("<tr><td><h3>" + data.image.title + "</h3></td></tr>");	
					$(data.image.messages).each(function (i, c) {
						$("#image").append("<tr><td>" + c + "</td></tr>")
					});
					$("#image").append("<tr><td><b>Score: " + data.image.rate + "</b></td></tr>");
	
					$("#w3c").html("<tr><td><h3>" + data.W3C.title + "</h3></td></tr>");	
					$(data.W3C.messages).each(function (i, c) {
						$("#w3c").append("<tr><td>" + c.message + "</td></tr>")
					});
					$("#w3c").append("<tr><td><b>Score: " + data.W3C.rate + "</b></td></tr>");

					$("#error").hide();
				} catch(err) {
					$("#error").show();
				}

			});
	});
});