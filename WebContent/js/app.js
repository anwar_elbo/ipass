function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

$(document).ajaxStart(function () {
	$(".grey-overlay").show();
}).ajaxStop(function () {
	$(".grey-overlay").hide();
});

$(document).ready(function() {
	var currentURL = window.location.href;
	if (currentURL.toLowerCase().indexOf("index.html") == -1 && currentURL.toLowerCase().indexOf("register.html") == -1)
	{
		$.ajax({
			url:'/SEOTool/rest/account/check',
			type:'get',
			async: false,
			success: function(data) {
				if(data == false){
					window.location.href = "index.html";
				}
			}
		});
	}
	
	$("#logout").click(function() {
		$.removeCookie("user_id");
		window.location.href = "index.html";
	});
});