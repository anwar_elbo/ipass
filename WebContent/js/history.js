$(document).ready(function() {
	var hid = getParameterByName('hid');
	var del = getParameterByName('delete');
	
	if(hid == null)
	{
		$.get(
			'/SEOTool/rest/account/history',
			function(data) 
			{
				$(data).each(function (i, c) {
					$("#history").append("<tr><td><h3>" + c.website + " - " + c.date.substring(0, 10) + "</h3></td></tr><tr style='word-wrap: break-word'><td>" + c.content + "<a href='history.html?hid=" + c.id + "'>Lees meer</a></td></tr><tr><td>&nbsp;</td></tr>")
				});
			});
	} else {
		if(del != 1) {
			$.get(
				'/SEOTool/rest/account/history/' + hid,
				function(data) 
				{	
					$("#history").append("<tr><td><h2>" + data[0].website + " - " + data[0].date.substring(0, 10) + "</h2></td></tr><tr><td><a href='history.html?hid=" + data[0].id + "&delete=1'>Delete</a></h2></td></tr>")
					$(data).each(function (i, c) {
						$("#history").append("<tr><td><h3>" + c.rate.name + "<h3></td></tr><tr><td>" + c.content + "</td></tr><tr><td>&nbsp;</td></tr>")
					});
				});
		} else {
			$.get(
				'/SEOTool/rest/account/history/delete/' + hid,
				function(data) 
				{
					window.location.href = "history.html";
				});
		}
	}
});