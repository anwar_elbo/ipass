$(document).ready(function() {
	$("#login").click(function() {
		$.ajax({
			url:'/SEOTool/rest/account/verify',
			async:false,
			type:'post', 
			data:$("#login_form").serialize(),
			success:function(data)
			{
				if(data != false) {
					$.cookie("user_id", data);
					window.location.href = "profile.html";
				} else {
					$("#error").show();
				}
			}
		});
	});
});