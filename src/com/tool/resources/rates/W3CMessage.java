package com.tool.resources.rates;

import java.text.Normalizer;

public class W3CMessage {
	private String message;
	private String type;
	
	public W3CMessage(String msg, String tp)
	{
		// get rid of non-ASCII characters
		message = Normalizer.normalize(msg, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");;
		type = tp;
	}
	
	public String getType()
	{
		return type;
	}
	
	public String getMessage()
	{
		return message;
	}
	
}
