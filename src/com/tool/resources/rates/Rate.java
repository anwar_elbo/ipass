package com.tool.resources.rates;

import java.util.ArrayList;

public interface Rate {
	int rate = 0;
	ArrayList<Object> messages = new ArrayList<Object>();
	final String TITLE = "";
	
	public int getRate();
	public String getTitle();
	public String getName();
	public ArrayList<Object> getMessages();
	
}
