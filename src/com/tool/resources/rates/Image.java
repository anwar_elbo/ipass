package com.tool.resources.rates;

import java.util.ArrayList;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Image implements Rate {
	private final String TITLE = "Beoordeling van plaatjes:";
	private final String NAME = "Images";
	private Elements images;
	private int rate = 0;
	private ArrayList<Object> messages = new ArrayList<Object>();

	public Image(Document doc) {
		this.images = doc.select("img");
	}

	@Override
	public int getRate() {
		rate = 0;
		if (images.size() > 0) {
			int maxPointsPerImage = 80 / images.size();
			rate += 10;
			if (images.size() >= 2) {
				messages.add("Je hebt genoeg plaatjes.");
				rate += 10;
			} else {
				messages.add("Probeer minimaal 2 plaatjes te hebben.");
			}

			for (Element e : images) {
				String alt = e.attr("alt");
				String title = e.attr("title");
				String src = e.attr("src");

				if (alt.length() != 0) {
					rate += maxPointsPerImage / 2;
				} else {
					messages.add("Alt tag bestaat niet voor het plaatje: " + src);
				}

				if (title.length() != 0) {
					rate += maxPointsPerImage / 2;
				} else {
					messages.add("Title tag bestaat niet voor het plaatje: " + src);
				}
			}
		} else {
			messages.add("Er zijn geen plaatjes op deze pagina.");
		}
		return rate;
	}

	@Override
	public ArrayList<Object> getMessages() {
		messages.clear();
		this.getRate();
		return messages;
	}

	@Override
	public String getTitle() {
		return TITLE;
	}
	
	public String getName() {
		return NAME;
	}

}
