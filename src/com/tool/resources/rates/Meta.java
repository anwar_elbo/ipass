package com.tool.resources.rates;

import java.util.ArrayList;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Meta implements Rate{

	private final String TITLE = "Beoordeling van de metatags:";
	private final String NAME = "Meta";
	private int rate;
	private ArrayList<Object> messages = new ArrayList<Object>();
	private Elements metaTag;
	private ArrayList<String> names = new ArrayList<String>();
	
	public Meta(Document doc)
	{
		metaTag = doc.select("meta");	
	}

	public int getRate()
	{
		for(Element e: metaTag)
		{	
			String content = e.attr("content").toLowerCase();
			if(!names.contains(e.attr("name")))
			{
				names.add(e.attr("name"));
				switch(e.attr("name"))
				{
					case "description":
							if(content.length() <= 155)
							{
								
								if(content.length() >= 120)
								{
									messages.add("De beschrijving is perfect!");
									rate += 65;
								} else {
									messages.add("De beschrijving is te kort probeer meer dan 120 characters te gebruiken.");
									rate += 35;
								}
								
							} else	{
								messages.add("De beschrijving is te lang probeer minder of gelijk aan 155 characters te gebruiken.");
								rate += 20;
							}
							break;
							
					case "keywords":
							if(content.split(",").length <= 5)
							{
								messages.add("Je gebruikt voldoende zoekwoorden!");
								rate += 5;
							} else {
								messages.add("Je gebruikt te veel zoek worden probeer minder dan 6 zoekwoorden te gebruiken.");
							}
							break;
							
					case "robots":
							if(content == "all")
							{
								messages.add("Je laat robots alles doen!");
								rate += 10;
							} else {
								for(String r : content.split(","))
								{
									if(r.trim() == "index")
									{
										messages.add("Je laat robots de website indexeren!");
										rate += 5;
									} else if(r.trim() == "follow")
									{
										messages.add("Je laat robots de website volgen!");
										rate += 5;
									}
								}
							}
							break;
							
					case "revisit-after":
							messages.add("Je hebt de revisit-after tag gebruikt!");
							rate += 15;
							break;
							
					case "generator":
							messages.add("Je hebt de generator tag gebruikt!");
							rate += 5;
							break;
				}
			}
		}
		return rate;
	}

	@Override
	public ArrayList<Object> getMessages() {
		return messages;
	}

	@Override
	public String getTitle() {
		return TITLE;
	}
	
	public String getName() {
		return NAME;
	}
	
	
}
