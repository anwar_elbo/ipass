package com.tool.resources.rates;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.*;

public class W3C implements Rate{
	private final String TITLE = "W3C validatie:";
	private final String NAME = "W3C";
	
	private String website;
	private ArrayList<Object> messages = new ArrayList<Object>();
	private int rate = 100;
	
	public W3C(String wbst) {
		this.website = wbst;

		messages.clear();
		try {
			String website = URLEncoder.encode(this.website, "UTF-8");
			URL url = new URL("https://validator.w3.org/nu/?doc=" + website + "&out=json");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
		
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
		
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			JSONObject obj = new JSONObject(br.readLine());
			JSONArray msg = obj.getJSONArray("messages");
		
			int max = 15;
			for (int i = 0; i < msg.length(); i++)
			{
			    String type = msg.getJSONObject(i).getString("type");
			    String message = msg.getJSONObject(i).getString("message");
			    messages.add(new W3CMessage(message, type));
			    if(i == max) {
					break;
				}
			}
		  } catch (Exception e) {
		
			e.printStackTrace();
		
		  }
	}

	public int getRate() {
		rate = 0;
		if(messages.size() == 0)
		{
			//messages.add("Je hebt geen W3C fouten!");
			rate = 100; 
		} else if(messages.size() < 5)
		{
			rate = 80;
		} else if(messages.size() < 10)
		{
			rate = 60;
		} else if(messages.size() < 20)
		{
			rate = 40;
		} else if(messages.size() < 30)
		{
			rate = 20;
		} else {
			rate = 0;
		}
		return rate;
	}

	@Override
	public ArrayList<Object> getMessages() {
		return messages;
	}
	
	@Override
	public String getTitle() {
		return TITLE;
	}
	
	public String getName() {
		return NAME;
	}

}
