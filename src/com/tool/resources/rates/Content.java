package com.tool.resources.rates;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Content implements Rate{
	private final String TITLE = "Beoordeling van de inhoud:";
	private final String NAME = "Content";
	private String text;
	private Document html;
	private int rate = 0;
	private ArrayList<Object> messages = new ArrayList<Object>();
	
	public Content(Document doc)
	{
		Elements content = doc.select("body");
		text = content.text();
		html = Jsoup.parse(content.html());
		
	}
	
	public int getRate() 
	{
		rate = 0;
		int totalContent = text.split("\\s+").length;
		int boldCount = html.select("b, strong").size();
		int italicCount = html.select("em, i").size();
		
		int aCount = html.select("a").size();
		
		int h1Count = html.select("h1").size();
		int h2Count = html.select("h2").size();
		int h3Count = html.select("h3").size();
		
		// Count anchor tags
		if(aCount != 0 && totalContent / aCount > 250)
		{
			if(totalContent / aCount < 150)
			{
				messages.add("Je hebt genoeg links!");
				rate += 15;
			} else {
				rate += 5;
				messages.add("Probeer een link te hebben voor elk 200 woorden. Je hebt te veel.");
			}
		} else {
			messages.add("Probeer een link te hebben voor elk 200 woorden. Je hebt te weinig.");
		}
		
		// Count H1 tags in content
		if(h1Count == 1)
		{
			rate += 5;
			messages.add("Je hebt een H1 kop!");
		} else {
			messages.add("Probeer alleen een H1 kop te gebruiken.");
		}
		
		// Count H2 tags in content
		if(h2Count > 0 )
		{
			messages.add("Je hebt minimaal een H2 kop gebruikt!");
			rate += 5;
		} else {
			messages.add("Probeer minimaal een H2 kop te gebruiken.");
		}
		
		// Count H3 tags in content
		if(h3Count > 0)
		{
			messages.add("Je hebt minimaal een H3 kop gebruikt!");
			rate += 5;
		} else {
			messages.add("Probeer minimaal een H3 kop te gebruiken.");
		}
		
		// Italic rate
		if(italicCount != 0 && totalContent / italicCount > 220)
		{
			if(totalContent / italicCount < 120)
			{
				messages.add("Je hebt genoeg schuingedrukte woorden.");
				rate += 20;
			} else {
				rate += 5;
				messages.add("Probeer een schuingedrukte woord te hebben voor elk 170 woorden. Je hebt te veel.");
			}
		} else {
			messages.add("Probeer een schuingedrukte woord te hebben voor elk 170 woorden. Je hebt te weinig.");
		}
		
		// Bold rate
		if(boldCount != 0 && totalContent / boldCount > 120)
		{
			if(totalContent / boldCount < 80)
			{
				messages.add("Je hebt genoeg dikgedrukte woorden.");
				rate += 20;
			} else {
				rate += 5;
				messages.add("Probeer een dikgedrukte woord te hebben voor elk 100 woorden. Je hebt te veel.");
			}
		} else {
			messages.add("Probeer een dikgedrukte woord te hebben voor elk 100 woorden. Je hebt te weinig.");
		}
		
		
		// Content rate
		if(totalContent >= 250)
		{
			rate += 20;
			if(totalContent >= 500)
			{
				messages.add("Je maakt gebruik van meer dan 500 woorden!");
				rate += 10;
			} else {
				messages.add("Goed, maar probeer meer dan 500 woorden te gebruiken.");
			}
		} else {
			messages.add("Te weinig inhoud probeer minimaal 250 woorden te gebruiken.");
		}
		return rate;
	}

	public ArrayList<Object> getMessages() {
		messages.clear();
		this.getRate();
		return messages;
	}

	@Override
	public String getTitle() {
		return TITLE;
	}
	
	public String getName() {
		return NAME;
	}

}
