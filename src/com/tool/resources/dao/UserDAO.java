package com.tool.resources.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tool.resources.pojo.User;

public class UserDAO extends BaseDAO {

	private List<User> findUser(String query) {
		List<User> results = new ArrayList<User>();

		try {
			Statement stmt = conn.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);

			while (dbResultSet.next()) {
				int userId = dbResultSet.getInt("id");
				String username = dbResultSet.getString("username");
				String email = dbResultSet.getString("email");
				String website = dbResultSet.getString("website");

				results.add(new User(userId, username, email, website));
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		return results;
	}

	public User getByUserId(int userId) {
		return findUser("SELECT * FROM `users` WHERE `id` = " + userId + " LIMIT 1").get(0);
	}

	public User getUserByLogIn(String username, String password) {
		try {
			return findUser("SELECT * FROM `users` WHERE `username` = '" + username + "' AND password = '" + password
					+ "' LIMIT 1").get(0);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	public void registerUser(String username, String password, String email, String website) {
		String query = "INSERT INTO `users` (username, password, email, website) VALUES ('" + username + "', '" + password + "', '" + email + "', '" + website + "')";

		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}
}
