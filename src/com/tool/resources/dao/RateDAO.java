package com.tool.resources.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tool.resources.pojo.Rate;

public class RateDAO extends BaseDAO {

	private List<Rate> findRate(String query) {
		List<Rate> results = new ArrayList<Rate>();

		try {
			Statement stmt = conn.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);

			while (dbResultSet.next()) {
				int rateId = dbResultSet.getInt("id");
				String rateNm = dbResultSet.getString("name");
				results.add(new Rate(rateId, rateNm));
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		return results;
	}

	public Rate getByRateId(int rateId) {
		return findRate("SELECT * FROM `rates` WHERE `id` = " + rateId + " LIMIT 1").get(0);
	}

	public Rate getByRateName(String name) {
		return findRate("SELECT * FROM `rates` WHERE `name` = '" + name + "' LIMIT 1").get(0);
	}

	public List<Rate> getAllRates() {
		return findRate("SELECT * FROM `rates`");
	}
}
