package com.tool.resources.dao;

import java.sql.*;

public class BaseDAO {
	public static final String DB_DRIV = "com.mysql.jdbc.Driver";
	public static final String DB_URL = "jdbc:mysql://localhost:3306/seo_tool";
	public static final String DB_USER = "root";
	public static final String DB_PASS = "";
	public static Connection conn;

	static {
		try {
			Class.forName(DB_DRIV).newInstance();
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e2) {
			e2.printStackTrace();
		} catch (ClassNotFoundException e3) {
			e3.printStackTrace();
		}

		try {
			conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}
}