package com.tool.resources.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tool.resources.pojo.History;
import com.tool.resources.pojo.Rate;
import com.tool.resources.pojo.User;

public class HistoryDAO extends BaseDAO {

	private List<History> findHistory(String query) {
		List<History> results = new ArrayList<History>();

		try {
			Statement stmt = conn.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);

			while (dbResultSet.next()) {
				// History object
				int hisId = dbResultSet.getInt("id");
				String content = dbResultSet.getString("content");
				String wbs = dbResultSet.getString("website");
				int score = dbResultSet.getInt("score");
				String date = dbResultSet.getString("created_date");

				// User object
				int usrId = dbResultSet.getInt("user_id");
				String username = dbResultSet.getString("username");
				String email = dbResultSet.getString("email");
				String website = dbResultSet.getString("website");

				// Rate object
				int rId = dbResultSet.getInt("rate_id");
				String name = dbResultSet.getString("name");

				results.add(new History(hisId, new Rate(rId, name), new User(usrId, username, email, website), content,
						score, wbs, date));
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		return results;
	}

	public List<History> getByUserId(String userId) {
		return findHistory(
				"SELECT t1.id, t1.website, t1.content, t1.score, t1.created_date, t2.id as user_id, t2.username, t2.email, t2.website, t3.id as rate_id, t3.name FROM `history` as t1 LEFT JOIN `users` as t2 ON t1.user_id = t2.id "
						+ "LEFT JOIN `rates` as t3 ON t1.rate_id = t3.id WHERE user_id = " + userId
						+ " GROUP BY t1.created_date ORDER BY t1.created_date DESC LIMIT 15");
	}

	public List<History> getByHistoryId(int hisId) {
		return findHistory(
				"SELECT t1.id, t1.website, t1.content, t1.score, t1.created_date, t2.id as user_id, t2.username, t2.email, t2.website, t3.id as rate_id, t3.name FROM `history` as t1 LEFT JOIN `users` as t2 ON t1.user_id = t2.id"
						+ " LEFT JOIN `rates` as t3 ON t1.rate_id = t3.id WHERE t1.created_date = (SELECT t4.created_date FROM `history` as t4 WHERE t4.id = "
						+ hisId + " LIMIT 1) LIMIT 4");
	}

	public void saveHistory(Rate rt, User usr, String cnt, String wbs, int score) {
		String query = "INSERT INTO `history` (rate_id, user_id, content, score, website, created_date) VALUES ("
				+ rt.getId() + ", " + usr.getUserId() + ", '" + cnt + "', " + score + ", '" + wbs
				+ "', DATE_FORMAT(NOW(),'%Y-%m-%e %H:%i:00'))";
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}

	public boolean deleteByHistoryId(int hisId) {
		int i = 0;
		String query = "DELETE FROM `history` WHERE created_date = (SELECT t1.created_date FROM (SELECT * FROM `history`) AS t1 WHERE id = "
				+ hisId + " LIMIT 1) LIMIT 4";
		try {
			Statement stmt = conn.createStatement();
			i = stmt.executeUpdate(query);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		if (i > 0) {
			return true;
		}

		return false;
	}
}
