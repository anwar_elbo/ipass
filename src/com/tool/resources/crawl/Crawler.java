package com.tool.resources.crawl;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.tool.resources.dao.HistoryDAO;
import com.tool.resources.dao.RateDAO;
import com.tool.resources.rates.Content;
import com.tool.resources.rates.Image;
import com.tool.resources.rates.Meta;
import com.tool.resources.rates.Rate;
import com.tool.resources.rates.W3C;

public class Crawler {
	private HashMap<String, Rate> results = new HashMap<String, Rate>();

	private String website;
	private String protocol;
	private Document doc;

	private Meta m;
	private Content c;
	private Image i;
	private W3C w;

	public Crawler(String wbst) {
		this.website = wbst;
	}

	public HashMap<String, Rate> crawl() {
		try {
			website.replace("www.", "");
			if (!website.toLowerCase().contains("http://") && !website.toLowerCase().contains("https://")) {
				protocol = "http://" + website;
			}

			try {
				doc = Jsoup.connect(protocol).get();

				m = new Meta(doc);
				c = new Content(doc);
				i = new Image(doc);
				w = new W3C(protocol);

				results.put("meta", m);
				results.put("content", c);
				results.put("image", i);
				results.put("W3C", w);
			} catch (UnknownHostException e) {
				results.put("meta", null);
				results.put("content", null);
				results.put("image", null);
				results.put("W3C", null);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return results;
	}

}
