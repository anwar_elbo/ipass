package com.tool.resources.pojo;

public class Rate {
	private int id;
	private String name;
	
	public Rate(int rid, String nm) {
		id = rid;
		name = nm;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}
}
