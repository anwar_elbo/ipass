package com.tool.resources.pojo;

public class User {
	private int userId;
	private String username;
	private String email;
	private String website;

	public User(int uid, String usName, String eMl, String web) {
		userId = uid;
		username = usName;
		email = eMl;
		website = web;
	}
	
	public String getUserId() {
		return String.valueOf(userId);
	}

	public String getUsername() {
		return username;
	}

	public String getEmail() {
		return email;
	}

	public String getWebsite() {
		return website;
	}

}
