package com.tool.resources.pojo;

public class History {
	private int id;
	private Rate rate;
	private User user;
	private String content;
	private int score;
	private String website;
	private String date;

	public History(int hisId, Rate rt, User usr, String cnt, int scr, String wbs, String dt) {
		id = hisId;
		rate = rt;
		user = usr;
		content = cnt;
		score = scr;
		date = dt;
		website = wbs;
	}
	
	public int getId() {
		return id;
	}
	
	public Rate getRate() {
		return rate;
	}

	public User getUser() {
		return user;
	}

	public int getScore() {
		return score;
	}
	
	public String getWebsite() {
		return website;
	}
	
	public String getDate() {
		return date;
	}
	
	public String getContent() {
		return content;
	}

}
