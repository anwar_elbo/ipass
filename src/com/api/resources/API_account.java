package com.api.resources;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import com.tool.resources.pojo.History;
import com.tool.resources.pojo.User;

@Path("account")
public class API_account {

	private Account acc = new Account();

	@POST
	@Path("/verify/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response verify(@FormParam("username") String username, @FormParam("password") String password) {
		User usr = acc.userLogin(username, password);

		if (usr == null) {
			return Response.ok("false").build();
		} else {
			return Response.ok(usr.getUserId()).build();
		}
	}

	@POST
	@Path("/register/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response regist(@FormParam("username") String username, @FormParam("website") String website,
			@FormParam("email") String email, @FormParam("password") String password) {
		
		acc.registerUser(username, password, email, website);
		return Response.ok("true").build();
	}

	@GET
	@Path("/info/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response info(@CookieParam(value = "user_id") String userId) {
		try {
			return Response.ok(acc.getUserByUserId(Integer.valueOf(userId))).build();
		} catch (NumberFormatException e) {
			return Response.ok("false").build();
		}
	}

	@GET
	@Path("/check/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response check(@CookieParam(value = "user_id") String userId) {
		if (userId == null || userId.equals("0")) {
			return Response.ok("false").build();
		} else {
			return Response.ok(userId).build();
		}
	}

	@GET
	@Path("/history/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<History> history(@CookieParam(value = "user_id") String userId) {
		return acc.getHistoryByUser(acc.getUserByUserId(Integer.valueOf(userId)));
	}

	@GET
	@Path("/history/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<History> historyById(@PathParam("id") int hisId) {
		return acc.getHistoryById(hisId);
	}

	@GET
	@Path("/history/delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteHistoryById(@PathParam("id") int hisId) {
		return Response.ok(acc.deleteHistoryById(hisId)).build();
	}
}
