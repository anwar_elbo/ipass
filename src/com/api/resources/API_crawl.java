package com.api.resources;

import java.util.HashMap;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.tool.resources.rates.Rate;

@Path("crawl")
public class API_crawl {
	private Page p;

	@GET
	@Path("/page/{site}")
	@Produces(MediaType.APPLICATION_JSON)
	public HashMap<String, Rate> page(@PathParam("site") String site, @CookieParam(value = "user_id") String userId) {
		p = new Page(site, Integer.valueOf(userId));
		return p.getPageRatings();
	}

	@GET
	@Path("/meta/{site}")
	@Produces(MediaType.APPLICATION_JSON)
	public Rate meta(@PathParam("site") String site, @CookieParam(value = "user_id") String userId) {
		p = new Page(site, Integer.valueOf(userId));
		return p.getMetaRate();
	}

	@GET
	@Path("/content/{site}")
	@Produces(MediaType.APPLICATION_JSON)
	public Rate content(@PathParam("site") String site, @CookieParam(value = "user_id") String userId) {
		p = new Page(site, Integer.valueOf(userId));
		return p.getContentRate();
	}

	@GET
	@Path("/images/{site}")
	@Produces(MediaType.APPLICATION_JSON)
	public Rate images(@PathParam("site") String site, @CookieParam(value = "user_id") String userId) {
		p = new Page(site, Integer.valueOf(userId));
		return p.getImageRate();
	}

	@GET
	@Path("/w3c/{site}")
	@Produces(MediaType.APPLICATION_JSON)
	public Rate W3C(@PathParam("site") String site, @CookieParam(value = "user_id") String userId) {
		p = new Page(site, Integer.valueOf(userId));
		return p.getW3CRate();
	}
}
