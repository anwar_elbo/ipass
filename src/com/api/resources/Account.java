package com.api.resources;

import java.util.HashMap;
import java.util.List;

import com.tool.resources.dao.HistoryDAO;
import com.tool.resources.dao.RateDAO;
import com.tool.resources.dao.UserDAO;
import com.tool.resources.pojo.History;
import com.tool.resources.pojo.User;
import com.tool.resources.rates.Rate;
import com.tool.resources.rates.W3CMessage;

public class Account {
	private UserDAO uD = new UserDAO();
	private HistoryDAO hD = new HistoryDAO();
	private RateDAO rD = new RateDAO();


	public void registerUser(String username, String password, String email, String website) {
		uD.registerUser(username, password, email, website);
	}
	
	public boolean deleteHistoryById(int hisId) {
		return hD.deleteByHistoryId(hisId);
	}

	public User userLogin(String uname, String pword) {
		return uD.getUserByLogIn(uname, pword);
	}

	public User getUserByUserId(int userId) {
		return uD.getByUserId(userId);
	}

	public List<History> getHistoryByUser(User usr) {
		return hD.getByUserId(usr.getUserId());
	}

	public List<History> getHistoryById(int hisId) {
		return hD.getByHistoryId(hisId);
	}

	public void saveHistoryForUser(HashMap<String, Rate> rates, int userId, String wbs) {

		for (String key : rates.keySet()) {
			if (rates.get(key) != null) {
				int rate = rates.get(key).getRate();

				StringBuilder cnt = new StringBuilder();

				for (Object s : rates.get(key).getMessages()) {
					if (rates.get(key).getName() != "W3C") {
						cnt.append(String.valueOf(s) + " ");
					} else {
						cnt.append(String.valueOf(((W3CMessage) s).getMessage()) + " ");
					}
				}

				hD.saveHistory(rD.getByRateName(rates.get(key).getName()), uD.getByUserId(userId), cnt.toString(), wbs,
						rate);

			}
		}
	}

}
