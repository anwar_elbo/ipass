package com.api.resources;

import java.util.HashMap;

import com.tool.resources.crawl.Crawler;
import com.tool.resources.dao.HistoryDAO;
import com.tool.resources.rates.Rate;

public class Page {

	private HashMap<String, Rate> rates;
	private String site;
	private Crawler c;
	private Account acc;

	public Page(String site, int userId) {
		this.site = site;
		c = new Crawler(site);
		acc = new Account();

		rates = c.crawl();

		acc.saveHistoryForUser(rates, userId, site);
	}

	public HashMap<String, Rate> getPageRatings() {
		return rates;
	}

	public Rate getMetaRate() {
		return (Rate) rates.get("meta");
	}

	public Rate getContentRate() {
		return (Rate) rates.get("content");
	}

	public Rate getImageRate() {
		return (Rate) rates.get("image");
	}

	public Rate getW3CRate() {
		return (Rate) rates.get("W3C");
	}

	public String getSite() {
		return site;
	}

}
